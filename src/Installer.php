<?php

namespace IDDI\Installer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class Installer extends LibraryInstaller
{

    public function install(){
        $path=__DIR__.'/../../../iddi-project/';
        @mkdir($path);
        @mkdir($path.'images');
        chmod($path.'images',0777);
        @mkdir($path.'images/library');
        chmod($path.'images/library',0777);
        @mkdir($path.'images/originals');
        chmod($path.'images/originals',0777);
        @mkdir($path.'images/public');
        chmod($path.'images/public',0777);
        @mkdir($path.'config');
        chmod($path.'config',0775);
        @mkdir($path.'templates');
        @mkdir($path.'plugins');
        @mkdir($path.'assets');

    }
}