<?php

namespace IDDI\Installer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

class InstallerPlugin implements PluginInterface
{
    public function activate(Composer $composer, IOInterface $io)
    {
        $installer = new \IDDI\Installer\Installer($io, $composer);
        $composer->getInstallationManager()->addInstaller($installer);
    }


}